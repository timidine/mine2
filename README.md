# Mine2

#### 介绍
emmm,基于LL的刷矿机。。。（BDS）

#### 安装教程

1.  安装BDS
2.  按照流程安装LiteLoader
3.  将压缩包内所有文件放入plugins

#### 使用说明

1.  第一次开服自动创建配置文件（.\\plugins\\Mine2\\config.json）
2.  请自主修改配置文件
3.  修改过程请使用物品标准名例：(minecraft:tnt)
4.  修改完成请重启服务器自动读取配置  
