#include "pch.h"
#include "TMfile.h"
#include <iostream>
#include <fstream>

using namespace std;
using namespace std::filesystem;

bool TMfile::writeTo(const string& path, const string& text)
{
	ofstream outfi;
	outfi.open(path, ios::out);
	outfi << text;
	outfi.close();
	return true;
}

string TMfile::readFrom(const string& path)
{
	ofstream file;
	file.open(path, ios::in);
	stringstream text;
	text << file.rdbuf();
	string c = text.str().c_str();
	file.close();
	return c;
}

bool TMfile::exists(const string& path)
{
	return (_access(path.c_str(), 0) == 0 ? true : false);
}