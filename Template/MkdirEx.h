#pragma once
#include <iostream>
#include <direct.h>
#include <string>
#include <io.h>

using namespace std;


static bool MkdirEx(string dir)
{
	const char* strDirPath = dir.c_str();
	if (strlen(strDirPath) > MAX_PATH)
	{
		return false;
	}
	size_t ipathLength = strlen(strDirPath);
	size_t ileaveLength = 0;
	size_t iCreatedLength = 0;
	char szPathTemp[MAX_PATH] = { 0 };
	for (int i = 0; (NULL != strchr(strDirPath + iCreatedLength, '\\')); i++)
	{
		ileaveLength = strlen(strchr(strDirPath + iCreatedLength, '\\')) - 1;
		iCreatedLength = ipathLength - ileaveLength;
		strncpy(szPathTemp, strDirPath, iCreatedLength);
		_mkdir(szPathTemp);
	}
	if (iCreatedLength < ipathLength)
	{
		_mkdir(strDirPath);
	}
	return true;
}