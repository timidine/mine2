#pragma once
#include "pch.h"

class TMfile
{
public:
	static bool writeTo(const string& path, const string& text);
	static string readFrom(const string& path);
	static bool exists(const string& path);
private:

};